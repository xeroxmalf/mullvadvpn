###  Create the container
```
docker create --name=vpn \
--restart="always" \
--cap-add=NET_ADMIN \
--device /dev/net/tun \
-v </some/path/with/mullvad/configs/extracted>:/config \
xeroxmalf/mullvadvpn
```

### Container with port forwards
```
docker create --name=vpn \
--restart="always" \
--cap-add=NET_ADMIN \
--device /dev/net/tun \
-v </some/path/with/mullvad/configs/extracted>:/config \
-p port:port \
xeroxmalf/mullvadvpn
```
### Generating config on mullvad
Make sure to set to udp 53, and set to use IPs.

### Config changes
Change: `proto udp` to `proto udp4`
Add: `pull-filter ignore "route-ipv6"`
`pull-filter ignore "ifconfig-ipv6"`

### Using this container with other containers
`--net=container:vpn`
